module.exports = {
  all: true,
  checkCoverage: true,
  perFile: false,
  lines: 80,
  statements: 80,
  functions: 80,
  branches: 60,
  reporter: ["text", "html", "cobertura"],
  exclude: ["**/node_modules/**", "**/test/**"],
  extension: [".js"],
  include: ["src/**/*.js"]
};

