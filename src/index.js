const chai = require('chai');
const chaiHttp = require('chai-http');
const mocha = require('mocha');
const mochaJunitReporter = require('mocha-junit-reporter');
const nyc = require('nyc');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

module.exports = {
  chai,
  chaiHttp,
  mocha,
  mochaJunitReporter,
  nyc,
  sinon,
  sinonChai,
};
