# Nodejs Testing Common

This is a shared library that contains common testing dependencies and configurations for Node.js projects. It simplifies the setup process by providing a consistent set of tools and best practices for testing across different projects.

## Features

- Includes popular testing packages: `Chai`, `Chai-HTTP`, `Mocha`, `Mocha-JUnit-Reporter`, `nyc`, `Sinon`, and `Sinon-Chai`.
- Pre-configured `nyc.config.js` and `.mocharc.js` files that follow best practices.
- Easy to extend and customize configurations for your specific project needs.

## Installation

To install `@beepbeepgo/nodejs-testing-common` as a devDependency in your project, run:

```bash
npm i -D @beepbeepgo/nodejs-testing-common
```

## Usage

After installing the library, you can extend the configurations provided by the library in your project.

### Extending nyc.config.js

Create a local `nyc.config.js` file in your project and extend the base configuration:

```js
// nyc.config.js
const baseConfig = require('@beepbeepgo/nodejs-testing-common/nyc.config.js');

module.exports = {
  ...baseConfig,
  // Add or override settings here
};
```

### Extending .mocharc.js

Create a local `.mocharc.js` file in your project and extend the base configuration:

```js
// .mocharc.js
const baseConfig = require('@beepbeepgo/nodejs-testing-common/.mocharc.js');

module.exports = {
  ...baseConfig,
  // Add or override settings here
};
```

## Customization

You can customize the extended configurations by adding or overriding settings specific to your project. For example, you may want to change the reporter options or add additional plugins.

### Customizing nyc.config.js

Let's say you want to exclude a specific folder from the coverage report. You can customize the `exclude` property in your local `nyc.config.js`:

```js
// nyc.config.js
const baseConfig = require('@beepbeepgo/nodejs-testing-common/nyc.config.js');

module.exports = {
  ...baseConfig,
  exclude: [...baseConfig.exclude, 'custom-folder/**'],
};
```

This will extend the base configuration's exclude array and add your 'custom-folder' to the list of excluded paths.

### Customizing .mocharc.js

Suppose you want to use a different timeout for your tests. You can customize the `timeout` property in your local .`mocharc.js`:

```js
// .mocharc.js
const baseConfig = require('@beepbeepgo/nodejs-testing-common/.mocharc.js');

module.exports = {
  ...baseConfig,
  timeout: 10000, // Set timeout to 10 seconds
};
```

This will override the base configuration's timeout value with the new value of 10000 milliseconds (10 seconds).
